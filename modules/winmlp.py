#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 FarmOS, Inc.
# All right reserved.
#

import time
import sys 
import pickle

sys.path.insert(0, "/usr/local/src/fcore")
from mblock import CmdCode
from code import RetCode, VarCode, AlertCode, AlertGrade
from result import ModResult, AlertResult
from variable import Variable

def windowtest(inputs, pvalues, dbcur):
    """
    inputs: intemp, outtemp
    outputs: cmd, worktime
    """
    cmdset = {0 : CmdCode.OFF, 301: CmdCode.TIMED_OPEN, 302: CmdCode.TIMED_CLOSE}

    model = pickle.load(open("/home/farmos/farmos_samples/aisample/model.mlp", "rb"))

    x = [[inputs["#intemp1"].getvalue(), inputs["#outtemp1"].getvalue()]]

    predicted = model.predict(x)

    cmd = cmdset[predicted[0]] if predicted[0] in cmdset else CmdCode.OFF

    return (RetCode.OK, [cmd, 30], None)


if __name__ == '__main__':
    inputs = {"#std": Variable(12.0), "#intemp1": Variable(25.0), "#outtemp1": Variable (15.0)}
    print (windowtest(inputs, None, None))
