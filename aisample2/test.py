import pickle

model = pickle.load(open("short.mlp", "rb"))
scaler = pickle.load(open("short.sc", "rb"))

data = [[16.400000000000006,53.0,763.7661019736842,284932.4529523025,48.67397550135685,100.0,100.0,100.0,23.213538655888787]]
inputs = scaler.transform(data)

predicted = model.predict(inputs)

print ("inputs", inputs)
print ("predicted", predicted)
