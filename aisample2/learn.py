import pandas as pd
import pickle
  
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import train_test_split
from sklearn import metrics

#dataset = pd.read_csv("./model_data.csv",index_col='obs_time',parse_dates=['obs_time'])
dataset = pd.read_csv("./short_data.csv",index_col='obs_time',parse_dates=['obs_time'])

inputs = dataset[['outtemp', 'outhum', 'rad', 'cumrad','upper_left', 'shade', 'warmer', 'side_cur', 'side']]
targets = dataset[[ 'intemp', 'inhum', 'co2']]

trainX, testX, trainY, testY = train_test_split(inputs, targets, test_size=0.2)

sc = StandardScaler()
scaler = sc.fit(trainX)
pickle.dump(scaler, open("short.sc", "wb"))

trainX_scaled = scaler.transform(trainX)
testX_scaled = scaler.transform(testX)

mlp_reg = MLPRegressor(hidden_layer_sizes = (15,10,50), max_iter = 1000, activation = 'relu', solver = 'adam')
mlp_reg.fit(trainX_scaled, trainY)

y_pred = mlp_reg.predict(testX_scaled)

pickle.dump(mlp_reg, open("short.mlp", "wb"))

print('Mean Absolute Error:', metrics.mean_absolute_error(testY, y_pred))
print('Mean Squared Error:', metrics.mean_squared_error(testY, y_pred))
                                                                           
