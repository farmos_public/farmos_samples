#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 FarmOS, Inc.
# All right reserved.
#

import time
import sys 
import pickle
from enum import IntEnum
from flask import Flask, request, json

app = Flask(__name__)

class CmdCode(IntEnum):
    OFF = 0                 # STOP
    ON = 201                # no param
    TIMED_ON = 202          # param : time (sec)
    DIRECTIONAL_ON = 203    # param : time (sec), ratio(-100 to 100)
    OPEN = 301              # no param
    CLOSE = 302             # no param
    TIMED_OPEN = 303        # param : time (sec)
    TIMED_CLOSE = 304       # param : time (sec)
    POSITION = 305          # param : position (0 to 100)
    SET_TIME = 306          # param : opentime, closetime

@app.route("/test", methods=['POST'])
def test():
    cmdset = {0 : CmdCode.OFF, 301: CmdCode.TIMED_OPEN, 302: CmdCode.TIMED_CLOSE}

    model = pickle.load(open("../aisample/model.mlp", "rb"))

    params = request.get_json()
    print("Inputs", params)

    x = [[params["data"]["intemp1"], params["data"]["outtemp1"]]]

    predicted = model.predict(x)

    cmd = cmdset[predicted[0]] if predicted[0] in cmdset else CmdCode.OFF

    response = {
        "status": "success",
        "results": [cmd, 30]
    }

    return json.dumps(response)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=8553)

