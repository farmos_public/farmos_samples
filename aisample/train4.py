#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 FarmOS, Inc.
# All right reserved.
#
# Classifier Sample
#

import pandas as pd
import pickle
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPRegressor
from sklearn import metrics

df = pd.read_csv('data.csv')
df = df.dropna()
x = df.drop(columns=['std','intemp'])
y = df['intemp']

trainX, testX, trainY, testY = train_test_split(x, y, test_size = 0.2)

sc = StandardScaler()
scaler = sc.fit(trainX)
trainX_scaled = scaler.transform(trainX)
testX_scaled = scaler.transform(testX)

clf = MLPRegressor(solver='adam', hidden_layer_sizes=(15,30), max_iter=10000)
clf.fit(trainX_scaled, trainY)

pred = clf.predict(testX_scaled)
dfpred = pd.DataFrame({'Actual': testY, 'Predicted': pred})
dfpred.head()

print('Mean Absolute Error:', metrics.mean_absolute_error(testY, pred))
print('Mean Squared Error:', metrics.mean_squared_error(testY, pred))

pickle.dump(clf, open("model.mlp", "wb"))
