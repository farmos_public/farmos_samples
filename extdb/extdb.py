#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 FarmOS, Inc.
# All right reserved.
#

import json
import time
import random
import pickle
from enum import IntEnum

from datamng import DBManager

def extdbmodel(option):
    dbm = DBManager(option)
    dbm.connect()
    intemp = dbm.loadobservation(2288)[0]["obs"]
    outtemp = dbm.loadobservation(2294)[0]["obs"]

    cmdset = {0 : CmdCode.OFF, 301: CmdCode.TIMED_OPEN, 302: CmdCode.TIMED_CLOSE}
    model = pickle.load(open("../aisample/model.mlp", "rb"))
    x = [[intemp, outtemp]]
    predicted = model.predict(x)

    cmd = cmdset[predicted[0]] if predicted[0] in cmdset else CmdCode.OFF
    opid = random.randint(1, 10000)
    dbm.sendrequest(2284, cmd.value, {"time": 30}, opid)

    while True:
        res = dbm.checkresponse(2284, opid)
        if len(res) > 0:
            break

        time.sleep(1)

    dbm.close()

if __name__ == "__main__":
    option = {
        "db": {
            "host": "localhost",
            "user": "farmos",
            "password": "rdachoi@@##",
            "db": "exfarmos"
        }
    }

    extdbmodel(option)

