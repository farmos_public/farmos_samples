#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 FarmOS, Inc.
# All right reserved.
#

import time
import json
import pymysql

class DBManager:
    def __init__(self, option):
        self._option = option
        self._conn = None

    def connect(self):
        copt = self._option["db"]
        self._conn = pymysql.connect(host=copt["host"], user=copt["user"],
                         password=copt["password"], db=copt["db"], 
                         cursorclass=pymysql.cursors.DictCursor, use_unicode=True, charset='utf8')
        self._cur = self._conn.cursor()

    def close(self):
        self._cur.close()
        self._conn.close()

    def loadobservation(self, senid, n=1):
        query = "select OBSR_DT tm, OBSR_VALUE obs, INSERT_DT intm, ESTIMATED_VALUE est from TB_CNTC_FARMOS_EQPMN_OBSR_VALUE_H where INSTL_EQPMN_ID = %s order by OBSR_DT desc limit %s"
        self._cur.execute(query, [senid, n])
        rows = self._cur.fetchall()
        self._conn.commit()
        return rows

    def loaddevstatus(self, devid, n=1):
        query = "select RECPTN_DT tm, STTUS_VALUE status, STTUS_ETC_CN extra from TB_CNTC_FARMOS_EQPMN_STTUS_H where INSTL_EQPMN_ID = %s order by RECPTN_DT desc limit %s"
        self._cur.execute(query, [devid, n])
        rows = self._cur.fetchall()
        self._conn.commit()
        return rows

    def sendrequest(self, devid, cmd, params, opid):
        query = "insert TB_CNTC_FARMOS_EQPMN_CTRL_CMMND_M(CMMND_ID, INSTL_EQPMN_ID, CMMND_VALUE, CMMND_ETC_CN, REQUST_DT) values (%s, %s, %s, %s, now())"
        ret = self._cur.execute(query, [opid, devid, cmd, json.dumps(params)])
        self._conn.commit()
        print ("sendreq", ret, query, [opid, devid, cmd, json.dumps(params)])

    def checkresponse(self, devid, opid):
        query = "select TRNSMIS_DT sendtm, RSPNS_DT restm, RSPNS_STTUS_VALUE res, RSPNS_ETC_CN extra from TB_CNTC_FARMOS_EQPMN_CTRL_CMMND_RSPNS_M where CMMND_ID = %s and INSTL_EQPMN_ID = %s"  
        self._cur.execute(query, [opid, devid])
        rows = self._cur.fetchall()
        self._conn.commit()
        return rows

if __name__ == "__main__":
    option = {
        "db": {
            "host": "localhost",
            "user": "farmos",
            "password": "rdachoi@@##",
            "db": "exfarmos"
        }
    }

    dm = DBManager(option)
    dm.connect()
    print (dm.loadobservation(2288, 3))
    print (dm.loaddevstatus(2278,2))

    dm.sendrequest(2278, 0, {}, 124)
    print ("right now", dm.checkresponse(2284, 7679))
    time.sleep(3)
    print ("after 3 seconds", dm.checkresponse(2278, 124))
    dm.close()
